# methylationpanelpipeline

1) Align reads with BWA-meth
2) SAM to sorted, dupmarked BAM
3) Call methylation with MethylDackel


TODO:

- Create capture target for picard/coverage
- Create CG-pos file for coverage/calling
- Create db to store:
     - AlignmentMetrics
     - HybridSelectionMetrics
     - InsertSizeMetrics
     - Calls per CG
- Write script for QC per CG-pos/target
