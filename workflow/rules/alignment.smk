rule fastq_trim_adapter:
    input:
        R1=INPUT + "/{sample}.R1.fastq.gz",
        R2=INPUT + "/{sample}.R2.fastq.gz"
    output:
        R1=temp(OUTPUT + "/{sample}.trimmed.R1.fastq.gz"),
        R2=temp(OUTPUT + "/{sample}.trimmed.R2.fastq.gz")
    conda:
        'envs/fastp.yaml'
    resources:
        mem_mb=4000,
        threads=1
    params:
        log = LOGDIR
    log:
        LOGDIR + "/{sample}.fastp.log"
    shell:
        '''
        fastp -i {input.R1} -I {input.R2} -o {output.R1} -O {output.R2} --verbose > {log}  2>&1
        '''


rule fastq_alignment:
    input:
        rules.fastq_trim_adapter.output.R1,
        rules.fastq_trim_adapter.output.R2
    output:
        temp(OUTPUT + "/{sample}.bam")
    conda:
        'envs/bwameth.yaml'
    envmodules:
        'samtools'
    resources:
        mem_mb=96000,
        threads=8
    params:
        log = LOGDIR,
        rg = "@RG\\tID:{sample}\\tLB:{sample}\\tPL:ILLUMINA\\tPU:{sample}\\tSM:{sample}"
    log:
        LOGDIR + "/{sample}.bwameth.log"        
        
    shell:
        '''
        (bwameth.py --threads {resources.threads} --read-group "{params.rg}" --reference {REF} {input} |\
        samtools view -@{resources.threads} -Sb |\
        samtools sort -@{resources.threads} - -o {output}) > {log}  2>&1
        '''


rule bam_markdups:
    input:
        rules.fastq_alignment.output
    output:
        bam = OUTPUT + "/output/{sample}.DM.bam",
        metrics = OUTPUT + "/metrics/{sample}.dupmark.txt"
    conda:
        'envs/picard.yaml'
    envmodules:
        'java'
    resources:
        mem_mb=4000,
        threads=1
    params:
        tempdir = OUTPUT + "/temp/{sample}_MD",
        log = LOGDIR
    log:
        LOGDIR + "/{sample}.markdups.log"        
    shell:
        '''
        picard MarkDuplicates  I={input} O={output.bam}  \
        M={output.metrics} OPTICAL_DUPLICATE_PIXEL_DISTANCE=2500 \
        CREATE_INDEX=true TMP_DIR={params.tempdir} > {log}  2>&1
        '''
