rule bam_call_methylation:
    input:
       bam = rules.bam_markdups.output.bam
    output:
        bed = OUTPUT + "/output/{sample}.DM_CpG.bedGraph"
    conda:
        'envs/methyldackel.yaml'
    resources:
        mem_mb=8000,
        threads=1
    params:
        log = LOGDIR
    log:
        LOGDIR + "/{sample}.MethylDackel.log"
    shell:
        '''
        MethylDackel extract --mergeContext -q 20 -p 20 \
        --maxVariantFrac 0.20 --minOppositeDepth 20 --minDepth 50 \
        {REF} {input.bam} > {log}  2>&1
        '''
