rule bam_insertsize_metrics:
    input:
        bamfile=rules.bam_markdups.output.bam
    output:
        table = OUTPUT + "/metrics/{sample}.InsertSize.txt",
        pdf = OUTPUT + "/metrics/{sample}.InsertSize.pdf"
    conda:
        'envs/picard.yaml'        
    resources:
        mem_mb=4000,
        threads=1
    params:
        log = LOGDIR
    log:
        LOGDIR + "/{sample}.InsertSize.log"  
    shell:
        '''
        picard CollectInsertSizeMetrics R={REF} I={input.bamfile} \
        O={output.table} HISTOGRAM_FILE={output.pdf} > {log} 2>&1
        '''


rule bam_alignment_metrics:
    input:
        bamfile=rules.bam_markdups.output.bam
    output:
        txt = OUTPUT + "/metrics/{sample}.AlignmentMetrics.txt"
    conda:
        'envs/picard.yaml'         
    resources:
        mem_mb=4000,
        threads=1
    params:
        log = LOGDIR
    log:
        LOGDIR + "/{sample}.AlignmentMetrics.log"
    shell:
        '''
        picard CollectAlignmentSummaryMetrics R={REF} I={input.bamfile} \
        O={output.txt}  MAX_INSERT_SIZE=500 > {log} 2>&1
        '''


rule bam_capture_metrics:
    input:
        bamfile=rules.bam_markdups.output.bam
    output:
        txt=OUTPUT + "/metrics/{sample}.HSMetrics.txt"
    conda:
        'envs/picard.yaml'          
    resources:
        mem_mb=4000,
        threads=1
    params:
        log = LOGDIR
    log:        
        LOGDIR + "/{sample}.HSMetrics.log"        
    shell:
        '''
        picard CollectHsMetrics R={REF} I={input.bamfile} \
        O={output.txt} TI={TARGETPIC} BI={TARGETPIC} > {log} 2>&1
        '''


rule bam_RRBS_metrics:
    input:
        bamfile=rules.bam_markdups.output.bam
    output:
        OUTPUT + "/metrics/{sample}.rrbs_qc.pdf",
        OUTPUT + "/metrics/{sample}.rrbs_detail_metrics",
        OUTPUT + "/metrics/{sample}.rrbs_summary_metrics"
    conda:
        'envs/picard.yaml'
    resources:
        mem_mb=4000,
        threads=1
    params:
        log = LOGDIR,
        prefix=OUTPUT + "/metrics/{sample}"
    log:
        LOGDIR + "/{sample}.RRBSmetrics.log"
    shell:
        '''
        picard CollectRrbsMetrics \
        R={REF} I={input.bamfile} M={params.prefix} > {log} 2>&1
        '''
