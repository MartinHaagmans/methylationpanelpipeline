rule bed_filter_methylation:
    input:
       bed=rules.bam_call_methylation.output.bed
    output:
        bed="output/{sample}.APHP_CpG.bedGraph"
    log:
        "logfiles/{sample}.BedTools.log"
    envmodules:
        'bedtools' 
    shell:
        '''
        bedtools intersect -wa -wb -a {input.bed} -b {TARGETCG} > {output.bed} {log}  2>&1
        '''
