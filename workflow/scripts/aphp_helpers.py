import sqlite3
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import xlsxwriter
import seaborn as sns
from math import ceil

sns.set_style("darkgrid")

class CollectMetrics:
    def __init__(self, db, sample):
        conn = sqlite3.connect(db)
        self.c = conn.cursor()
        self.conn = conn
        self.sample = sample
        
    def get_alignmetrics(self):
        r1_sql = f'''SELECT TOTAL_READS, MEAN_READ_LENGTH, SD_READ_LENGTH 
            FROM alignment WHERE (SAMPLE="{self.sample}" AND CATEGORY="FIRST_OF_PAIR")
            '''
        r2_sql = f'''SELECT TOTAL_READS, MEAN_READ_LENGTH, SD_READ_LENGTH 
            FROM alignment WHERE (SAMPLE="{self.sample}" AND CATEGORY="SECOND_OF_PAIR")
            '''
        self.c.execute(r1_sql)
        r1 = self.c.fetchone()
        r1_total, r1_mean, r1_stdev = r1
        
        self.c.execute(r2_sql)
        r2 = self.c.fetchone()
        r2_total, r2_mean, r2_stdev = r2

        output = dict()
        output['R1_total'] = r1_total
        output['R1_length_mean'] = round(r1_mean)
        output['R1_length_sd'] = round(r1_stdev)
        output['R2_total'] = r2_total
        output['R2_length_mean'] = round(r2_mean)
        output['R2_length_sd'] = round(r2_stdev)
        return output
    
    def get_hsmetrics(self):    
        hsmetrics_sql = f'''SELECT PCT_PF_UQ_READS, PCT_SELECTED_BASES, MEAN_BAIT_COVERAGE, 
            MEAN_TARGET_COVERAGE, PCT_TARGET_BASES_100X, PCT_TARGET_BASES_250X, 
            PCT_TARGET_BASES_500X, PCT_TARGET_BASES_1000X, PCT_TARGET_BASES_2500X 
            FROM hsmetrics WHERE SAMPLE="{self.sample}"
            '''        
        self.c.execute(hsmetrics_sql)
        pct_uniq_reads, pct_ontarget, mean_bait, mean_target, x100, x250, x500, x1000, x2500 = self.c.fetchone()
        output = dict()
        output['PCT_UNIQ'] = round(100*pct_uniq_reads)
        output['PCT_ONTARGET'] = round(100*pct_ontarget)
        output['MEAN_BAIT_COV'] = round(mean_bait)
        output['MEAN_TARGET_COV'] = round(mean_target)
        output['% > 100X'] = round(100*x100)
        output['% > 250X'] = round(100*x250)
        output['% > 500X'] = round(100*x500)
        output['% > 1000X'] = round(100*x1000)
        output['% > 2500X'] = round(100*x2500)
        return output
        
    def get_insert_size_metrics(self):    
        insert_size_sql = f'''SELECT MEDIAN_INSERT_SIZE, MEAN_INSERT_SIZE, STANDARD_DEVIATION
            FROM insertsize WHERE SAMPLE="{self.sample}"
            '''        
        self.c.execute(insert_size_sql)
        median, mean, stdev = self.c.fetchone()
        output = dict()
        output['MEDIAN_INSERT_SIZE'] = median
        output['MEAN_INSERT_SIZE'] = round(mean)
        output['STDEV_INSERT_SIZE'] = round(stdev)
        return output
    
    def get_perc_converted(self):    
        insert_size_sql = f'''SELECT PCT_NON_CPG_BASES_CONVERTED, PCT_CPG_BASES_CONVERTED
            FROM rrbsmetrics WHERE SAMPLE="{self.sample}"
            '''        
        self.c.execute(insert_size_sql)
        noncpg, cpg = self.c.fetchone()
        output = dict()
        output['PCT_NON_CPG_BASES_CONVERTED'] = round(100*noncpg)
        output['PCT_CPG_BASES_CONVERTED'] = round(100*cpg)
        return output      

    def get_all(self):
        hsmetrics =self.get_hsmetrics()
        insmetrics = self.get_insert_size_metrics()
        rrbsmetrics = self.get_perc_converted()
        metrics = pd.DataFrame((hsmetrics | insmetrics | rrbsmetrics), index=[self.sample])
        metrics = metrics.transpose()
        metrics.columns = ['']        
        return metrics        
        
class Methylation:

    def __init__(self, methyldackel_bedgraph, sample, control_pickle_norm, control_pickle_raw):
        self.bedgraph = methyldackel_bedgraph
        self.sample = sample
        self.per_region = pd.read_pickle(control_pickle_norm)
        self.sample_data = self.parse_methyl_dackel_output()
        self.zscores = self.get_zscores()
        self.ctr_raw = pd.read_pickle(control_pickle_raw)
        self.z_upper = 3
        self.z_lower = -3

        self.targets = {
            'diagnostic': 
                ['GNAS-A/B_D', 'GNAS-AS1_D', 'GNAS-NESP_D', 'GNAS-XL_D',
                 'GRB10_D', 'H19_D', 'KCNQ1_D', 'MAGEL2_D', 'MEG3_D', 'MEG8_D',
                 'MEST_D', 'NDN_D', 'PEG10_D', 'PLAGL1_D', 'SNRPN_D'
                 ],
            'research': 
                ['BLCAP_R', 'DIRAS3_R', 'DSCAM_R', 'ERLIN2_R', 'FAM50B_R', 'FANCC_R',
                 'GET1_R', 'GPR1_R', 'HM13_R', 'HTR5A_R', 'IG-DMR_R', 'IGF1R_R', 'IGF2R_R',
                 'IGF2_R', 'INPP5F_R', 'L3MBTL1_R', 'MKRN3_R', 'NAP1L5_R', 'PEG13_R', 'PEG3_R',
                 'PPIEL_R', 'RB1_R', 'SIM2_R', 'SNU13_R', 'SVOPL_R', 'VTRNA2_R', 'WDR27_R', 'ZNF331_R', 'ZNF597_R'
                 ]
            }

        self.target_count = {
             'BLCAP_R': 133,
             'DIRAS3_R': 121,
             'DSCAM_R': 47,
             'ERLIN2_R': 36,
             'FAM50B_R': 89,
             'FANCC_R': 26,
             'GET1_R': 50,
             'GNAS-A/B_D': 198,
             'GNAS-AS1_D': 127,
             'GNAS-NESP_D': 253,
             'GNAS-XL_D': 199,
             'GPR1_R': 369,
             'GRB10_D': 170,
             'H19_D': 244,
             'HM13_R': 41,
             'HTR5A_R': 54,
             'IG-DMR_R': 53,
             'IGF1R_R': 53,
             'IGF2R_R': 74,
             'IGF2_R': 92,
             'INPP5F_R': 49,
             'KCNQ1_D': 190,
             'L3MBTL1_R': 84,
             'MAGEL2_D': 48,
             'MEG3_D': 188,
             'MEG8_D': 41,
             'MEST_D': 220,
             'MKRN3_R': 102,
             'NAP1L5_R': 52,
             'NDN_D': 106,
             'PEG10_D': 116,
             'PEG13_R': 189,
             'PEG3_R': 217,
             'PLAGL1_D': 141,
             'PPIEL_R': 37,
             'RB1_R': 194,
             'SIM2_R': 314,
             'SNRPN_D': 124,
             'SNU13_R': 61,
             'SVOPL_R': 32,
             'VTRNA2_R': 79,
             'WDR27_R': 50,
             'ZNF331_R': 221,
             'ZNF597_R': 103
            }
    
    def parse_methyl_dackel_output(self):
        df = pd.read_csv(
            self.bedgraph, 
            sep='\t', header=None, usecols=[0, 1, 2, 3, 4, 5, 9], index_col=[0, 1, 2],
            names=['chrom', 'start', 'end', f'{self.sample}_perc', f'{self.sample}_reads_meth', f'{self.sample}_reads_unmeth', 'ID'],
            dtype={0: str, 1: int, 2: int, 3: int, 4: int, 5: int, 6: str}
        )
        return df
    
    def get_zscores(self, normalize=True):
        if normalize:
            _mean = self.sample_data[f'{self.sample}_perc'].mean()
            self.sample_data[f'{self.sample}_norm'] = self.sample_data[f'{self.sample}_perc'].div(_mean)
            df = self.sample_data.join(self.per_region).dropna()
            df['Z'] = (df[f'{self.sample}_norm'] - df['mean']) / df['std']
        elif not normalize:
            df = self.sample_data.join(self.per_region).dropna()
            df['Z'] = (df[f'{self.sample}_perc'] - df['mean']) / df['std']
        return df

    def get_calls(self):
        calls = self.zscores[
            (self.zscores['Z'] > self.z_upper) | (self.zscores['Z'] < self.z_lower)
        ]
        return calls

    def scatterplot(self):
        
        colors = sns.color_palette("husl", len(self.zscores['ID'].unique()))
        max_axis = ceil(self.zscores[f'{sample}_norm'].max())
        
        i = 0
        
        fig = plt.figure(figsize=(12,9))
        
        for target, data in self.zscores.groupby('ID'):
            plt.scatter(data['mean'], data[f'{self.sample}_norm'], label=target, color=colors[i], s=10)
            i += 1
        
        plt.xlabel('Archive normalized', size=16)
        plt.ylabel(f'{self.sample} normalized', size=16)
        plt.title(f'{self.sample} vs archive', size=22)
        plt.xlim(0, max_axis)
        plt.ylim(0, max_axis) 
        plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        plt.tight_layout()
        plt.savefig(f'{sample}_scatterplot.png', dpi=120)
        return
        
    def create_output_table(self):
        
        out = self.zscores.join(self.ctr_raw, lsuffix='_NORM', rsuffix='_RAW')
        out[f'DP'] = out[f'{self.sample}_reads_meth'] + out[f'{self.sample}_reads_unmeth']
        out = out.drop([f'{self.sample}_reads_meth', f'{self.sample}_reads_unmeth'], axis=1)
        rename = {
            f'{self.sample}_perc': 'PERC', 
            'ID': 'DMR', 
            f'{self.sample}_norm': 'PERC_NORM', 
            'mean_NORM': 'ARCH_MEAN_NORM', 
            'std_NORM': 'ARCH_STD_NORM',
            'Z': 'Z-score',	
            'mean_RAW':	'ARCH_MEAN', 
            'std_RAW': 'ARCH_STD'
        }

        out = out.rename(rename, axis=1)
        out = out[['DMR', 'PERC', 'PERC_NORM', 'Z-score', 'DP', 'ARCH_MEAN', 'ARCH_STD', 'ARCH_MEAN_NORM', 'ARCH_STD_NORM']]
        return out

    def create_output_summary(self):
        summary = list()

        targets_covered = dict()

        for target, data in self.zscores.groupby('ID'):
            targets_covered[target] = len(data)
        
        calls = self.get_calls()
        calls = calls[calls['ID'].isin(self.targets['diagnostic'])]

        calls = calls.join(self.ctr_raw, lsuffix='_NORM', rsuffix='_RAW')
        
        calls[f'DP'] = calls[f'{self.sample}_reads_meth'] + calls[f'{self.sample}_reads_unmeth']
        calls = calls.drop([f'{self.sample}_reads_meth', f'{self.sample}_reads_unmeth'], axis=1)
        rename = {
            f'{self.sample}_perc': 'PERC', 
            'ID': 'DMR', 
            f'{self.sample}_norm': 'PERC_NORM', 
            'mean_NORM': 'ARCH_MEAN_NORM', 
            'std_NORM': 'ARCH_STD_NORM',
            'Z': 'Z-score',	
            'mean_RAW':	'ARCH_MEAN', 
            'std_RAW': 'ARCH_STD'
        }
        calls = calls.rename(rename, axis=1)
        
        for dmr, data in calls.groupby('DMR'):
            data['delta'] = data['PERC'] - data['ARCH_MEAN']
            perc_targets_callable = 100*(targets_covered[dmr] / self.target_count[dmr])
            
            perc_targets_called = 100*(len(data) / targets_covered[dmr])
            summary.append([dmr, f'{perc_targets_callable:.4}', f'{perc_targets_called:.4}', f'{data["delta"].mean():.4}'])
        return summary
        
    def plot_calls(self, target_type=None):

        if target_type is None:
            target_type = 'diagnostic'

        calls = self.get_calls()
        calls = calls.join(self.ctr_raw, lsuffix='_NORM', rsuffix='_RAW')
        
        _colors = sns.color_palette("husl", len(self.targets[target_type]))
        colors = {name: _colors[i] for i, name in enumerate(self.targets[target_type])}    
        
        fig, (ax1, ax2, ax3) = plt.subplots(3, figsize=(18,9))

        
        x = 0
        
        targets_called = list(calls['ID'].unique())
        
        for target in self.targets[target_type]:
            
            targets_covered = self.zscores[self.zscores['ID'] == target]['ID'].count()
            
            if not target in targets_called:
                continue
            else:
                data = self.zscores[self.zscores['ID'] == target].join(self.ctr_raw, lsuffix='_NORM', rsuffix='_RAW')
                
            target_zscores = list(data['Z'].clip(-15, 15).values)
            target_sample = list(data[f'{self.sample}_perc'].values)
            target_norm_sample = list(data[f'{self.sample}_norm'].values)
            target_mean_archive = list(data['mean_RAW'].values)
            target_norm_mean_archive = list(data['mean_NORM'].values)

            data['mean_RAW+'] = data['mean_RAW'] + (3*data['std_RAW'])
            data['mean_RAW-'] = data['mean_RAW'] - (3*data['std_RAW'])
            
            data['mean_NORM+'] = data['mean_NORM'] + (3*data['std_NORM'])
            data['mean_NORM-'] = data['mean_NORM'] - (3*data['std_NORM'])            
            
            target_mean_archive_plus = list(data['mean_RAW+'].values)
            target_mean_archive_min = list(data['mean_RAW-'].values)

            target_norm_mean_archive_plus = list(data['mean_NORM+'].values)
            target_norm_mean_archive_min = list(data['mean_NORM-'].values)
            
            xm = x + len(target_zscores)
            perc_called = 100 * (len(calls[calls['ID'] == target]) / targets_covered) 
            ax1.plot(range(x,xm), target_zscores, '.', color=colors[target], label=f'{target}: {perc_called:.4}')
            ax2.plot(range(x,xm), target_sample, '.', color=colors[target])
            ax2.plot(range(x,xm), target_mean_archive_plus, color='grey', alpha=0.5)
            ax2.plot(range(x,xm), target_mean_archive_min, color='grey', alpha=0.5)
            
            ax3.plot(range(x,xm), target_norm_sample, '.', color=colors[target])
            ax3.plot(range(x,xm), target_norm_mean_archive_plus, color='grey', alpha=0.5)
            ax3.plot(range(x,xm), target_norm_mean_archive_min, color='grey', alpha=0.5)
            

        
            x += len(target_zscores)
        
        ax1.set_ylim(-16, 16)
        ax1.axhline(-3)
        ax1.axhline(3)
        ax2.set_ylim(0, 100)

        ax1.set_ylabel('Z-score', size=16)
        ax2.set_ylabel('Methylation \n percentage', size=16)
        ax3.set_ylabel('Normalized methylation \n percentage', size=16)
        
        for ax in [ax1, ax2]:
            ax.set_xticks([], [])
            ax.set_xlabel('')
        
        ax1.legend(loc='center left', bbox_to_anchor=(1, 0.5))

        plt.suptitle(f'{self.sample} {target_type} targets', size=22, x=0.4)
        
        plt.tight_layout()
        plt.savefig(f'{self.sample}_{target_type}_calls.png', dpi=120)
        return
