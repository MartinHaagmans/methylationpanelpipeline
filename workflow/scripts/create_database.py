#!/usr/bin/env python

import sqlite3

class MetricsDBcreator:
    """Create database tables for pipeline output.

    Every diagnostic test has a unique code that is linked to analyses to
    perform and target files to be used in given analyses.
    """

    def __init__(self, db):
        """Establish a connection with a database."""
        self.conn = sqlite3.connect(db)
        self.c = self.conn.cursor()

    def _execute_and_commit(self, statement):
        try:
            self.c.execute(statement)
        except sqlite3.OperationalError as e:
            print(e)
        else:
            self.conn.commit()

    def create_alignmentmetrics_table(self):
        """Create table to store alignent metrics."""
        sql = """CREATE TABLE alignment (
        SAMPLE TEXT NOT NULL,
        SERIE TEXT NOT NULL,
        CATEGORY TEXT NOT NULL,
        TOTAL_READS INTEGER NOT NULL,
        PF_READS INTEGER NOT NULL,
        PCT_PF_READS REAL NOT NULL,
        PF_NOISE_READS INTEGER NOT NULL,
        PF_READS_ALIGNED INTEGER NOT NULL,
        PCT_PF_READS_ALIGNED REAL NOT NULL,
        PF_ALIGNED_BASES INTEGER NOT NULL,
        PF_HQ_ALIGNED_READS INTEGER NOT NULL,
        PF_HQ_ALIGNED_BASES INTEGER NOT NULL,
        PF_HQ_ALIGNED_Q20_BASES INTEGER NOT NULL,
        PF_HQ_MEDIAN_MISMATCHES INTEGER NOT NULL,
        PF_MISMATCH_RATE REAL NOT NULL,
        PF_HQ_ERROR_RATE REAL NOT NULL,
        PF_INDEL_RATE REAL NOT NULL,
        MEAN_READ_LENGTH INTEGER NOT NULL,
        SD_READ_LENGTH INTEGER NOT NULL,
        MEDIAN_READ_LENGTH INTEGER NOT NULL,
        MAD_READ_LENGTH INTEGER NOT NULL,
        MIN_READ_LENGTH INTEGER NOT NULL,
        MAX_READ_LENGTH INTEGER NOT NULL,
        MEAN_ALIGNED_READ_LENGTH INTEGER NOT NULL,
        READS_ALIGNED_IN_PAIRS INTEGER NOT NULL,
        PCT_READS_ALIGNED_IN_PAIRS REAL NOT NULL,
        PF_READS_IMPROPER_PAIRS,
        PCT_PF_READS_IMPROPER_PAIRS,
        BAD_CYCLES INTEGER NOT NULL,
        STRAND_BALANCE REAL NOT NULL,
        PCT_CHIMERAS REAL NOT NULL,
        PCT_ADAPTER REAL NOT NULL,
        PCT_SOFTCLIP REAL NOT NULL,
        PCT_HARDCLIP REAL NOT NULL,
        AVG_POS_3PRIME_SOFTCLIP_LENGTH INTEGER NOT NULL,
        LIBRARY TEXT,
        READ_GROUP TEXT,
        PRIMARY KEY(SAMPLE,SERIE,CATEGORY))
        """
        self._execute_and_commit(sql)

    def create_hsmetrics_table(self):
        """Create table to store Hybrid Selection (HS) metrics."""
        sql = """CREATE TABLE hsmetrics (
        SAMPLE TEXT NOT NULL,
        SERIE TEXT NOT NULL,
        BAIT_SET TEXT NOT NULL,
        GENOME_SIZE INTEGER NOT NULL,
        BAIT_TERRITORY INTEGER NOT NULL,
        TARGET_TERRITORY INTEGER NOT NULL,
        BAIT_DESIGN_EFFICIENCY REAL NOT NULL,
        TOTAL_READS INTEGER NOT NULL,
        PF_READS INTEGER NOT NULL,
        PF_BASES INTEGER NOT NULL,
        PF_UNIQUE_READS INTEGER NOT NULL,
        PCT_PF_READS INTEGER NOT NULL,
        PCT_PF_UQ_READS INTEGER NOT NULL,
        PF_UQ_READS_ALIGNED INTEGER NOT NULL,
        PCT_PF_UQ_READS_ALIGNED REAL NOT NULL,
        PF_BASES_ALIGNED INTEGER NOT NULL,
        PF_UQ_BASES_ALIGNED INTEGER NOT NULL,
        ON_BAIT_BASES INTEGER NOT NULL,
        NEAR_BAIT_BASES INTEGER NOT NULL,
        OFF_BAIT_BASES INTEGER NOT NULL,
        ON_TARGET_BASES INTEGER NOT NULL,
        PCT_SELECTED_BASES REAL NOT NULL,
        PCT_OFF_BAIT REAL NOT NULL,
        ON_BAIT_VS_SELECTED REAL NOT NULL,
        MEAN_BAIT_COVERAGE REAL NOT NULL,
        MEAN_TARGET_COVERAGE REAL NOT NULL,
        MEDIAN_TARGET_COVERAGE INTEGER NOT NULL,
        MAX_TARGET_COVERAGE INTEGER NOT NULL,
        MIN_TARGET_COVERAGE INTEGER NOT NULL,
        PCT_USABLE_BASES_ON_BAIT REAL NOT NULL,
        PCT_USABLE_BASES_ON_TARGET REAL NOT NULL,
        FOLD_ENRICHMENT REAL NOT NULL,
        ZERO_CVG_TARGETS_PCT REAL NOT NULL,
        PCT_EXC_DUPE INTEGER NOT NULL,
        PCT_EXC_ADAPTER INTEGER NOT NULL,
        PCT_EXC_MAPQ REAL NOT NULL,
        PCT_EXC_BASEQ INTEGER NOT NULL,
        PCT_EXC_OVERLAP INTEGER NOT NULL,
        PCT_EXC_OFF_TARGET REAL NOT NULL,
        FOLD_80_BASE_PENALTY TEXT NOT NULL,
        PCT_TARGET_BASES_1X REAL NOT NULL,
        PCT_TARGET_BASES_2X INTEGER NOT NULL,
        PCT_TARGET_BASES_10X INTEGER NOT NULL,
        PCT_TARGET_BASES_20X INTEGER NOT NULL,
        PCT_TARGET_BASES_30X INTEGER NOT NULL,
        PCT_TARGET_BASES_40X INTEGER NOT NULL,
        PCT_TARGET_BASES_50X INTEGER NOT NULL,
        PCT_TARGET_BASES_100X INTEGER NOT NULL,
        PCT_TARGET_BASES_250X INTEGER NOT NULL,
        PCT_TARGET_BASES_500X INTEGER NOT NULL,
        PCT_TARGET_BASES_1000X INTEGER NOT NULL,
        PCT_TARGET_BASES_2500X INTEGER NOT NULL,
        PCT_TARGET_BASES_5000X INTEGER NOT NULL,
        PCT_TARGET_BASES_10000X INTEGER NOT NULL,
        PCT_TARGET_BASES_25000X INTEGER NOT NULL,
        PCT_TARGET_BASES_50000X INTEGER NOT NULL,
        PCT_TARGET_BASES_100000X INTEGER NOT NULL,
        HS_LIBRARY_SIZE REAL,
        HS_PENALTY_10X INTEGER NOT NULL,
        HS_PENALTY_20X INTEGER NOT NULL,
        HS_PENALTY_30X INTEGER NOT NULL,
        HS_PENALTY_40X INTEGER NOT NULL,
        HS_PENALTY_50X INTEGER NOT NULL,
        HS_PENALTY_100X INTEGER NOT NULL,
        AT_DROPOUT INTEGER NOT NULL,
        GC_DROPOUT REAL NOT NULL,
        HET_SNP_SENSITIVITY REAL NOT NULL,
        HET_SNP_Q INTEGER NOT NULL,
        LIBRARY TEXT,
        READ_GROUP TEXT,
        PRIMARY KEY(SAMPLE,SERIE))
        """
        self._execute_and_commit(sql)

    def create_insertsizemetrics_table(self):
        """Create table to store insert size metrics."""
        sql = """CREATE TABLE insertsize (
        SAMPLE TEXT NOT NULL,
        SERIE TEXT NOT NULL,
        MEDIAN_INSERT_SIZE INTEGER NOT NULL,
        MODE_INSERT_SIZE INTEGER NOT NULL,
        MEDIAN_ABSOLUTE_DEVIATION INTEGER NOT NULL,
        MIN_INSERT_SIZE INTEGER NOT NULL,
        MAX_INSERT_SIZE INTEGER NOT NULL,
        MEAN_INSERT_SIZE REAL NOT NULL,
        STANDARD_DEVIATION REAL NOT NULL,
        READ_PAIRS INTEGER NOT NULL,
        PAIR_ORIENTATION TEXT NOT NULL,
        WIDTH_OF_10_PERCENT INTEGER NOT NULL,
        WIDTH_OF_20_PERCENT INTEGER NOT NULL,
        WIDTH_OF_30_PERCENT INTEGER NOT NULL,
        WIDTH_OF_40_PERCENT INTEGER NOT NULL,
        WIDTH_OF_50_PERCENT INTEGER NOT NULL,
        WIDTH_OF_60_PERCENT INTEGER NOT NULL,
        WIDTH_OF_70_PERCENT INTEGER NOT NULL,
        WIDTH_OF_80_PERCENT INTEGER NOT NULL,
        WIDTH_OF_90_PERCENT INTEGER NOT NULL,
        WIDTH_OF_95_PERCENT INTEGER NOT NULL,
        WIDTH_OF_99_PERCENT INTEGER NOT NULL,
        LIBRARY TEXT,
        READ_GROUP TEXT,
        PRIMARY KEY(SAMPLE,SERIE))
        """
        self._execute_and_commit(sql)

    def create_base_perc_reads(self):
        """Create table to store per base percentages of R1."""
        sql = """CREATE TABLE basepercentages
        (SAMPLE TEXT NOT NULL,
        SERIE TEXT NOT NULL,
        A REAL NOT NULL,
        T REAL NOT NULL,
        C REAL NOT NULL,
        G REAL NOT NULL,
        PRIMARY KEY(SAMPLE, SERIE))
        """
        self._execute_and_commit(sql)

    def create_all(self):
        to_create = [
            self.create_alignmentmetrics_table(),
            self.create_hsmetrics_table(),
            self.create_insertsizemetrics_table(),
            self.create_base_perc_reads(),
        ]

        for func in to_create:
                func

if __name__ == '__main__':

    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-db", "--database", type=str, 
        help="Naam database", required=True
        )
    args = parser.parse_args()

    db = args.database                        
    MetricsDBcreator(db).create_all()                .
