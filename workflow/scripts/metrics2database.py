#!/usr/bin/env python
import sqlite3
import pandas as pd
from pathlib import Path

class MetricsToDB:

    def __init__(self, snakemake_params):
        # self.sampleID = snakemake_params.sample
        # self.serie = snakemake_params.serie
        # self.metrics_doc = snakemake_params.doc
        # self.metrics_alignment = snakemake_params.alignment
        # self.metrics_capture = snakemake_params.hsmetrics
        # self.metrics_insertsize = snakemake_params.insertsize
        # self.metrics_rrbs = snakemake_params.rrbs
        # self.conn, self.c = self._connect_db(snakemake_params.db)
        # self.output = snakemake_params.output
        self.sampleID = snakemake_params['sample']
        self.serie = snakemake_params['serie']
        self.metrics_alignment = snakemake_params['alignment']
        self.metrics_capture = snakemake_params['hsmetrics']
        self.metrics_insertsize = snakemake_params['insertsize']
        self.metrics_rrbs = snakemake_params['rrbs']
        self.conn, self.c = self._connect_db(snakemake_params['db'])
        self.output = snakemake_params['output']

    def _connect_db(self, db):
        conn = sqlite3.connect(db)
        c = conn.cursor()
        return (conn, c)

    def _get_line(self, fn, phrase):
        """Read file. Report line number that starts with phrase and the first
        blank line after that line. Return tuple of integers
        """
        start = 0
        end = 0
        with open(fn, 'r') as f:
            output = tuple()
            for i, line in enumerate(f):
                if line.startswith(phrase):
                    start = i
                if line in ['\n', '\r\n'] and start > 0:
                    end = i
                    return (start, end)

    def parse_picard_metrics(self, fn):
        "Read block (line x through y) from file with pandas. Return dataframe."
        start, end = self._get_line(fn, '## METRICS CLASS')
        df = pd.read_csv(fn, sep='\t', header=start, nrows=end - start - 2)
        df['SAMPLE'] = self.sampleID
        df['SERIE'] = self.serie
        df.set_index(['SAMPLE', 'SERIE'], inplace=True)
        return df

    def picard_metrics_2db(self, df, table):
        try:
            df.to_sql(table, self.conn, if_exists='append')
        except sqlite3.OperationalError as e:
            print(e)
        return        
        
    def all_to_database(self):
        data_alignment = self.parse_picard_metrics(self.metrics_alignment)
        data_hsmetrics = self.parse_picard_metrics(self.metrics_capture)
        data_insertsize = self.parse_picard_metrics(self.metrics_insertsize)
        data_rrbsmetrics = self.parse_picard_metrics(self.metrics_rrbs)
        
        self.picard_metrics_2db(data_alignment, 'alignment')
        self.picard_metrics_2db(data_hsmetrics, 'hsmetrics')
        self.picard_metrics_2db(data_insertsize, 'insertsize')
        self.picard_metrics_2db(data_rrbsmetrics, 'rrbsmetrics')
        self.conn.commit()
        self.conn.close()
        
        Path(self.output).touch()


# MetricsToDB(snakemake)all_to_database()