#!/usr/bin/env python
import sqlite3
import pandas as pd


class CollectData():
    def __init__(self, snakemake_params):
        conn = sqlite3.connect(snakemake_params.config.metrics_db)
        self.c = conn.cursor()
        self.conn = conn
        self.sample = snakemake_params.wildcards.sample
        
    def get_metrics_alignment(self):
        r1_sql = f'''SELECT TOTAL_READS, MEAN_READ_LENGTH, SD_READ_LENGTH 
            FROM alignment WHERE (SAMPLE="{self.sample}" AND CATEGORY="FIRST_OF_PAIR")
            '''
        r2_sql = f'''SELECT TOTAL_READS, MEAN_READ_LENGTH, SD_READ_LENGTH 
            FROM alignment WHERE (SAMPLE="{self.sample}" AND CATEGORY="SECOND_OF_PAIR")
            '''
        self.c.execute(r1_sql)
        r1 = self.c.fetchone()
        r1_total, r1_mean, r1_stdev = r1
        
        self.c.execute(r2_sql)
        r2 = self.c.fetchone()
        r2_total, r2_mean, r2_stdev = r2

        output = dict()
        output['R1_total'] = r1_total
        output['R1_length_mean'] = round(r1_mean)
        output['R1_length_sd'] = round(r1_stdev)
        output['R2_total'] = r2_total
        output['R2_length_mean'] = round(r2_mean)
        output['R2_length_sd'] = round(r2_stdev)
        return output
    
    def get_metrics_basepercentages(self):
        bases = f'SELECT A, T, C, G FROM basepercentages WHERE SAMPLE="{self.sample}"'
        self.c.execute(bases)
        a, t, c, g = self.c.fetchone()
        output = dict()
        output['perc_A'] = round(100*a)
        output['perc_T'] = round(100*t)
        output['perc_C'] = round(100*c)
        output['perc_G'] = round(100*g)
        return output
    
    def get_metrics_capture(self):    
        hsmetrics_sql = f'''SELECT PCT_PF_UQ_READS, PCT_SELECTED_BASES, MEAN_BAIT_COVERAGE, 
            MEAN_TARGET_COVERAGE, PCT_TARGET_BASES_100X, PCT_TARGET_BASES_250X, 
            PCT_TARGET_BASES_500X, PCT_TARGET_BASES_1000X, PCT_TARGET_BASES_2500X 
            FROM hsmetrics WHERE SAMPLE="{self.sample}"
            '''        
        self.c.execute(hsmetrics_sql)
        pct_uniq_reads, pct_ontarget, mean_bait, mean_target, x100, x250, x500, x1000, x2500 = self.c.fetchone()
        output = dict()
        output['PCT_UNIQ'] = round(100*pct_uniq_reads)
        output['PCT_ONTARGET'] = round(100*pct_ontarget)
        output['MEAN_BAIT_COV'] = round(mean_bait)
        output['MEAN_TARGET_COV'] = round(mean_target)
        output['% > 100X'] = round(100*x100)
        output['% > 250X'] = round(100*x250)
        output['% > 500X'] = round(100*x500)
        output['% > 1000X'] = round(100*x1000)
        output['% > 2500X'] = round(100*x2500)
        return output
        
    def get_metrics_insert_size(self):    
        insert_size_sql = f'''SELECT MEDIAN_INSERT_SIZE, MEAN_INSERT_SIZE, STANDARD_DEVIATION
            FROM insertsize WHERE SAMPLE="{self.sample}"
            '''        
        self.c.execute(insert_size_sql)
        median, mean, stdev = self.c.fetchone()
        output = dict()
        output['MEDIAN_INSERT_SIZE'] = median
        output['MEAN_INSERT_SIZE'] = round(mean)
        output['STDEV_INSERT_SIZE'] = round(stdev)
        return output
    
    def get_metrics_rrbs(self):    
        rrbs_sql = f'''SELECT PCT_NON_CPG_BASES_CONVERTED, PCT_CPG_BASES_CONVERTED
            FROM rrbsmetrics WHERE SAMPLE="{self.sample}"
            '''        
        self.c.execute(rrbs_sql)
        noncpg, cpg = self.c.fetchone()
        output = dict()
        output['PCT_NON_CPG_BASES_CONVERTED'] = round(100*noncpg)
        output['PCT_CPG_BASES_CONVERTED'] = round(100*cpg)
        return output    

    def get_all(self):
        
        align = self.get_metrics_alignment()
        base = self.get_metrics_basepercentages()
        hsmetrics = self.get_metrics_capture()
        insmetrics = self.get_metrics_insert_size()
        rrbsmetrics = self.get_metrics_rrbs()

        df = pd.DataFrame((align | base | hsmetrics | insmetrics | rrbsmetrics), index=[self.sample])
        df = df.transpose()
        df.columns = ['']
        return df